#include <iostream>
#include <string> 
#include "cadastro_socio.hpp"
#include "cadastro_cliente.hpp"
using namespace std;
template <typename var>
var leitura (){
    while(true){
    var lido;
    cin >> lido; 
    if(cin.fail()){
        cin.clear();
        cin.ignore(32776, '\n');
        cout << "Entrada Inválida\n"; 
    }
    else 
    return lido; 
    }
} 
int escolha_opcao(){
    int escolha; 
    while(true){
        system("clear");
        cout << "CADASTRO DE SÓCIO" <<endl << endl; 
        cout << "(1) Cliente já existente" << endl << "(2) Cliente novo" << endl << "(0) Voltar" << endl; 
        escolha  = leitura<int>(); 
        system("clear");
        if(escolha < 3 && escolha > -1) 
            break;
    }
    return escolha; 
}
Socio cadastro_ce(vector <Cliente *> clientes){
    Socio socio;
    int numero_de_clientes = clientes.size(),i; 
    long int cpf;
    string nome, email, senha;
    float total_de_compras; 
    while(true){
        cout << "Digite cpf do cliente: "; 
        cpf = leitura<long int>();
        for(i =0;i<numero_de_clientes;i++){
            if(cpf == clientes[i]->get_cpf()){
                if(clientes[i]->eh_socio() == "sim"){
                    cout << "Sócio já cadastro" << endl; 
                    return socio = Socio(); 
                }
                else{
                total_de_compras = clientes[i]->get_total_de_compras() ;
                nome = clientes[i]->get_nome();
                email = clientes[i]->get_email();
                cout << "Cadastre a senha: ";
                senha = lerstring();
                system("clear"); 
                clientes[i]->set_nome(""); 
                 int qntd_categorias_do_cliente = clientes[i]->categorias_existentes(); 
                    vector <int> qntd_comprada_categorias; 
                    for(int w=0;w<qntd_categorias_do_cliente;w++){
                        qntd_comprada_categorias.push_back(clientes[i]->get_categorias_compradas(w));
                    } 
                cout << "Sócio Cadastrado" << endl; 
                return socio = Socio(nome, cpf, email, qntd_comprada_categorias, total_de_compras,"01/10/2019",senha); 
                }
            }
        }
            int cond=0,saida;     
            while(true){
                cout << "Cliente não encontrado" << endl << endl;
                cout << "(1) Tentar novamente" << endl << "(0) Sair" << endl; 
                saida = leitura<int>(); 
                system("clear"); 
                if(saida == 1 )
                    break; 
                else if(saida == 0){
                    cond = 1;
                    break;
                }
                else
                    cout << "Entrada Inválida" << endl; 
            }
            if(cond ==1)
                break; 
        }
    return socio = Socio(); 
}
Socio cadastro_ci(vector<Cliente *> clientes){
    int numero_de_clientes = clientes.size(),i; 
    Cliente cliente = cadastre(clientes);
    Socio socio;
    string senha,nome,email;
    float total_de_compras; 
    if(cliente.get_nome()==""){
        for(i=0;i<numero_de_clientes;i++){
            if(cliente.get_cpf() == clientes[i]->get_cpf()){
                if(clientes[i]->eh_socio()=="sim"){
                    cout << "Sócio já cadastrado" << endl; 
                    return socio = Socio(); 
                }
                else {
                    nome = clientes[i]->get_nome();
                    email = clientes[i]->get_email();
                    total_de_compras= clientes[i]->get_total_de_compras();
                    cout << "Resgatando informações do cliente ..." << endl << endl; 
                    cout << "Cadastre uma senha: ";
                    senha = lerstring();
                    system("clear");  
                    clientes[i]->set_nome("");
                    int qntd_categorias_do_cliente = clientes[i]->categorias_existentes(); 
                    vector <int> qntd_comprada_categorias; 
                    for(int w=0;w<qntd_categorias_do_cliente;w++){
                        qntd_comprada_categorias.push_back(clientes[i]->get_categorias_compradas(w));
                    } 
                    cout << "Sócio Cadastrado" << endl;  
                    return socio = Socio(nome, cliente.get_cpf(), email,qntd_comprada_categorias, total_de_compras, "01/10/2019", senha); 
                }
            }
        }
    }
    else {
        cout << "Senha: "; 
        cin >> senha; 
        system("clear");  
    }
    cout << "Sócio cadastrado" << endl;
    return socio = Socio(cliente.get_nome(), cliente.get_cpf(), cliente.get_email(),"01/10/2019", senha);
}