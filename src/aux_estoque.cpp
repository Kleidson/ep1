#include "aux_estoque.hpp"
#include "cadastro_cliente.hpp"
#include <iostream>
#include <vector>
using namespace std;
template <typename var> 
var leitura (var cond){
    while(true){
    var lido;
    cin >> lido; 
    if(cin.fail()||lido < cond){
        cin.clear();
        cin.ignore(32776, '\n');
        cout << "Entrada Inválida\n"; 
    }
    else 
    return lido; 
    }
}
void ok(int i){
    if(i==1)
    cout << "(0)CONFIRMAR" << endl; 
    else
       cout << "(0)VOLTAR" << endl; 
    int lixo; 
    while (true) {
        lixo = leitura <int> ();
        if(lixo ==0){
            system("clear");
            break;
        }
        else 
            cout << "Comando Inválido" << endl; 
    }
}
int edition(vector <Produto *> produtos ){
    int qntd_produtos = produtos.size(),guardar; 
   while (true) {
       system("clear"); 
    cout << "MODO ESTOQUE -- EDIÇÂO" << endl << endl << endl; 
    cout << "Escolha uma opção:"<< endl << endl; 
    cout << "(1)Editar um produto por busca" << endl << "(2)Verificação geral dos produtos" << endl << "(0)Sair do modo edição" <<endl;
        int opcao = leitura <int> (0); 
        system("clear");
        if(opcao == 1){
            while(true){
                system("clear"); 
                cout << "ATUALIZANDO ESTOQUE" << endl << endl << "(1)Iniciar busca" << endl << "(0)Sair" << endl;
                int busq = leitura<int>(0); 
                if(busq == 0)
                    break;
                else if(busq!=1)
                    cout << "Comando inválido" << endl; 
                else {
                    system("clear"); 
                    cout << "Busque um produto: ";
                    string nome= lerstring(); system("clear"); 
                    int i, cond =0; 
                    for( i=0;i<qntd_produtos;i++){
                        if(nome == produtos[i]->get_nproduto()){
                            cout << "Produto encontrado" << endl <<endl; 
                            cond = 1;
                            guardar = i;
                            break; 
                     }
                    }
                    if(cond == 0){
                        cout << "Produto não econtrado" << endl <<endl <<endl ; 
                        ok(2);
                    }
                    else{
                        while (true){
                            produtos[guardar]->imprime_dados(); 
                            cout << endl << endl << endl << "Escolha uma ação:" << endl;
                            cout << "(1)Atualizar estoque\n(2)Alterar valor\n(3)Remover do estoque\n(0)Voltar à busca" <<endl; 
                            int acao = leitura<int>(0);
                            system("clear") ; 
                            if(acao == 1){
                                cout << "Quantidade de itens para acrescentar: ";
                                long int qntd = leitura<long int>(1);
                                system("clear");  
                                produtos[guardar] ->set_qntd(produtos[guardar]->get_qntd()+qntd);
                                cout << "Estoque de " << produtos[guardar]->get_nproduto() << " atualizado" << endl <<endl ;
                                ok(2); 
                            }
                            else if(acao == 2){
                                cout << "Novo valor do produto: "; 
                                float preco = leitura<float>(0.1f); 
                                produtos[guardar] ->set_preco(preco); 
                                cout <<"Preço atualizado!" << endl<<endl; 
                                ok(2); 
                            }
                            else if(acao == 3){
                                while (true){
                                    cout <<"ATENÇÂO: Essa ação não poderá ser desfeita" <<endl;
                                    cout << "Tem certeza que deseja remover " << nome << " dos produtos?" << endl <<endl;
                                    cout << "(1)Sim  (2)Não, voltar" << endl; 
                                    int opt = leitura<int> (1);
                                    system("clear");
                                    if(opt==1){
                                        cout << "Produto deletado" << endl; 
                                        ok(2); 
                                        return guardar; 
                                    }
                                    else if(opt !=2)
                                        cout << "Comando inválido" << endl; 
                                    else
                                        break;      
                                }     
                            }
                            else if(acao == 0)
                                break; 
                            else
                                cout << "COMANDO INVÀLIDO" << endl; 
                        
                        }
                    }
                }
            }
        }
        else if(opcao == 2){
              vector <string> produtos_em_falta; 
              int j,aux; 
              for( j =0; j<qntd_produtos;j++){
                  if(produtos[j]->get_qntd()==0)
                    produtos_em_falta.push_back(produtos[j]->get_nproduto());
              } 
              aux = produtos_em_falta.size();
              if(aux == 0)
                cout << "Nenhum produto registrado está em falta" << endl;
            else {
              cout << "Essa é a lista dos produtos em falta:" << endl;
              for(j=0;j<aux;j++){
                  cout << "--" << produtos_em_falta[j] << endl; 
              }
            }
              cout << endl << endl; 
              ok(2); 
        }
        else if( opcao ==0)
            break; 
        else
            cout << "Comando Inválido!" << endl;  
   }
   return -1;
} 
Produto cadastre_p(vector <Produto *> produtos ){
     system("clear");
    Produto produto;
    cout << "Cadastrando um novo produto" << endl << endl; 
    cout << "Nome do produto: "; 
    string nome = lerstring();
    int qntd_produtos = produtos.size(); 
    for(int i = 0; i<qntd_produtos; i++){
        if(nome == produtos[i]->get_nproduto()){
            system("clear"); 
            cout << endl << endl <<"ERRO: produto já cadastrado" << endl; 
            cout << "Como opção, você pode atualizar o estoque" << endl << endl; 
            return produto = Produto();  
        }
    }
    cout << "Quantidade no estoque: "; 
    long int qntd = leitura<long int>(1); 
    cout << "Preço de venda: R$"; 
    float preco = leitura<float>(0.1);
    cout << "Categoria: "; 
    vector <string> categorias; 
    int a,qntd_categorias =0,i;
    while(true){
        int cond =0; 
        categorias.push_back(lerstring());
        qntd_categorias = categorias.size(); 
        for(i = 0; i< qntd_categorias-1; i++){
            if(categorias[qntd_categorias-1]==categorias[i]){
                cout << "Alerta: categoria repetida." << endl << "Tal categoria não será salva novamente" << endl; 
                categorias.erase(categorias.begin()+qntd_categorias-1); 
            }
        }
        while(true){
            cout << endl <<"Adicionar nova categoria? " << endl << endl <<"(1)SIM  (2)NÂO" << endl; 
            a = leitura <int> (0); 
            system("clear");
            if(a ==2){
                cond = 1; 
                break; 
            }
            else if (a==1){ 
                cout << "Nova categoria para " << nome << ": ";
                break;  
            }       
            else 
                cout << "Comando inválido! Tente novamente" << endl; 
        }
        if(cond ==1)
            break; 
    }
    system("clear");  
    cout << "Produto Cadastrado!" << endl << endl << endl; 
     return produto = Produto (nome, qntd, preco, categorias);
}

