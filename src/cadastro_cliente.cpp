#include <iostream>
#include <string> 
#include <vector>
#include "cadastro_cliente.hpp"
#include "aux_estoque.hpp"

using namespace std; 
template <typename var>

var leitura (){
    while(true){
    var lido;
    cin >> lido; 
    if(cin.fail()){
        cin.clear();
        cin.ignore(32776, '\n');
        cout << "Entrada Inválida\n"; 
    }
    else 
    return lido; 
    }
}
string lerstring(){
    string texto;
    cin.ignore();
     getline(cin,texto);
      
    return texto; 
}
Cliente cadastre(vector <Cliente *> clientes) {
    Cliente cliente;
    int i = clientes.size(), j;
     long int cpf; 
    system("clear");
    cout << "Cadastrando um novo cliente" << endl << endl; 
    cout << "Digite nome: "; 
    string nome = lerstring();
    while(true){
    cout << "Informe o CPF: ";
    cpf = leitura<long int>();
    if(cpf >= 100000000000 || cpf <1000000000)
        cout << "cpf inválido" << endl; 
    else break; 
    }
    for (j=0;j<i;j++){
        if(cpf == clientes[j]->get_cpf()){
            system("clear");
            cout  << "Cliente já possui um cadastro!" << endl << endl;
            return cliente = Cliente(cpf); 
        }
    }
    cout << "Digite o email: "; 
    string email = lerstring();
    return cliente = Cliente (nome, cpf,email);
}


Cliente novo_cadastro(vector<Carrinho *> carrinho, vector <Cliente *> clientes, vector <Produto *> produtos, vector<string>categorias) {
    Cliente cliente; 
    string nome, email;
    long int cpf; 
    float total_comprado= carrinho[0]->get_total_comprado(); 
    int qntd_clientes = clientes.size(), qntd_produtos = produtos.size(),qntd_itens = carrinho[0]->get_quantidade_itens(); 
    int qntd_categorias = categorias.size(); 
    cout << "CADASTRANDO NOVO CLIENTE" << endl << endl;
    cout << "Digite o nome: ";
    nome = lerstring();
    while(true){
    cout << "Informe o CPF: ";
    cpf = leitura<long int>();
    if(cpf >= 100000000000 || cpf <1000000000)
        cout << "cpf inválido" << endl; 
    else break; 
    }
    for(int i = 0; i< qntd_clientes; i++){
        if(clientes[i]->get_cpf() == cpf ){
            system("clear"); 
            cout << "Cliente já possui cadastro!" << endl << "Transferindo dados da compra para seu registro" << endl <<endl;
            if(clientes[i]->eh_socio()=="sim"){
                while(true){
                    cout << clientes[i]->get_nome() << ", digite sua senha para ganhar 15% de desconto na compra" << endl << "senha: ";
                    string senha_cliente = lerstring();
                    system("clear");
                    if(senha_cliente == clientes[i]->get_senha()){
                        total_comprado*=0.85;
                        cout << "Compra realizada" << endl << endl;  
                        break;
                    }
                    else {
                        int condicao = 0; 
                        while(true){
                            cout << "Senha Incorreta" << endl; 
                            cout << "(1)Tentar novamente" << endl << "(2)Continuar a compra sem desconto" << endl; 
                            int escolha = leitura<int> (); 
                            system("clear"); 
                            if(escolha == 1)
                                break; 
                            else if (escolha == 2){
                                cout << "Compra realizada" << endl; 
                                condicao = 1;
                                break; 
                            }
                            else
                                cout << "Comando Inválido" << endl; 
                        }
                        if(condicao == 1)
                            break; 
                    }
                }
            } 
            clientes[i]->set_total_de_compras(clientes[i]->get_total_de_compras()+total_comprado);
            for(int j = 0; j<qntd_itens ; j++){
                for(int k =0 ; k<qntd_produtos;k++){
                    if(produtos[k]->get_nproduto() == carrinho[0]->get_nome_produto(j)){
                        produtos[k]->set_qntd(produtos[k]->get_qntd()-carrinho[0]->get_qntd_compra(j));
                       int qntd_categoriasp = produtos[k]->qntd_de_categorias();  
                        for(int auxiliar = 0; auxiliar < qntd_categoriasp; auxiliar ++){
                            for(int auxiliar1=0;auxiliar1<qntd_categorias;auxiliar1++){
                                if(produtos[k]->get_categoria(auxiliar) == categorias[auxiliar1]){
                                    clientes[i]->set_categorias_compradas(auxiliar1, clientes[i]->get_categorias_compradas(auxiliar1)+carrinho[0]->get_qntd_compra(j));
                                }
                            }
                        }
                    }
                }
            }
            return cliente = Cliente();  
        }
    } 
    cout << "Digite o email: "; 
    email = lerstring();
    system("clear");
    return cliente = Cliente(nome,cpf,total_comprado,email); 

}
void atualizacao_clientes(vector<Cliente *> clientes, vector <string> categorias){
    int numero_categorias = categorias.size();
    int numero_clientes = clientes.size();
            for(int i=0;i<numero_clientes; i++){
                clientes[i]->adicionar_categoria(numero_categorias-clientes[i]->categorias_existentes());
            }
}

void edita_clientes(vector <Cliente *> clientes){
    
    int qntd_clientes = clientes.size(),cond=-1,guardar,parada=0; 
    long int cpf;
    while(true){
    while(true){
        cout << "(1)Buscar cliente" << endl << "(0)VOltar" << endl; 
        int busca = leitura<int> ();
         system("clear"); 
         if(busca == 1){
             cond =0;
             break; 
         }
         else if(busca == 0) { 
             parada =1;  
            break;   
         }
    }
    if(parada ==1 ) break; 
    if(cond == 0){
    while(true){
        cout << "Digite o cpf do cliente: ";
        cpf = leitura<long int> (); 
        system("clear"); 
        for (int i =0; i<qntd_clientes;i++){
            if(cpf == clientes[i]->get_cpf()){
                cond =1;
                guardar=i; 
                break; 
            }
        }
        if(cond == 0){       
            while(true){
                cout << "Cliente não cadastrado" << endl; 
                cout << "Tentar novamente?" << endl << endl << "(1) SIM" << endl << "(2) NÂO, SAIR" << endl; 
                int escolha = leitura<int>();
                system("clear"); 
                if(escolha == 1)
                    break;
                else if(escolha == 2){
                    cond =2; 
                    parada =1; 
                    break; 
                }
            }
        }
        else if(cond == 1){
            while(true){
                clientes[guardar]->imprime_dados(); 
                cout << "(1)Alterar nome" << endl << "(2)Alterar email" << endl << "(0)Retornar" << endl; 
                int opcao_altera = leitura<int>();
                system("clear");
                if(opcao_altera == 1){
                    cout << "Informe o novo nome para " << clientes[guardar]->get_nome() << ": ";
                    string nome = lerstring(); 
                    system("clear");
                    clientes[guardar]->set_nome(nome);
                }
                else if(opcao_altera == 2){
                    cout << "Informe o novo email para "<< clientes[guardar]->get_nome() << ": ";
                    string email = lerstring();
                    system("clear");
                    clientes[guardar] ->set_email(email);
                }
                else if(opcao_altera == 0)
                    break; 
                
            }
            break;  
        }
        if(cond == 2) break;
    }
}
if(parada ==1 ) break; 
}  
}