#include <iostream>
#include <vector>
#include "aux_compra.hpp"
#include "cliente.hpp"
#include "produto.hpp"
#include "cadastro_cliente.hpp"
#include "carrinho.hpp"
#include "aux_estoque.hpp"
using namespace std; 
template <typename var>
var leitura (var cond){
    while(true){
    var lido;
    cin >> lido; 
    if(cin.fail() || lido <cond ){
        cin.clear();
        cin.ignore(32776, '\n');
        cout << "Entrada Inválida\n"; 
    }
    else 
    return lido; 
    }
}
using namespace std; 
void verificacao_de_cliente(vector <Cliente *> &clientes, vector <Produto *> produtos, vector < string> categorias) {
    while (true){
        cout << "Digite o cpf do cliente:";
        long int cpf = leitura<long int>();
        int cond =0,guardar,condicao_parada=0;
        system("clear"); 
        int qntd_clientes = clientes.size(); 
        for(int i =0; i< qntd_clientes; i++){
            if(clientes[i]->get_cpf()== cpf){
                guardar = i; 
                cond =1;
                break; 
            }
        }
        if(cond == 0){
            while(true){
                cout << "Cliente não cadastrado" << endl << endl << endl;
                cout << "(1)Tentar novamente" << endl << "(2)Cadastrar novo cliente" << endl << "(0)Sair" << endl; 
                int escolha = leitura<int>(); 
                system("clear");
                if(escolha == 1)
                    break; 
                if(escolha == 2){
                    clientes.push_back(new Cliente (cadastre(clientes))); 
                    qntd_clientes = clientes.size();
                    if(clientes[qntd_clientes-1]->get_nome() == ""){
                        clientes.pop_back();
                        ok(2);  
                    }
                    else 
                    atualizacao_clientes(clientes, categorias); 
                    system("clear");
                    break; 
                }
                if(escolha == 0){
                    condicao_parada =1;
                    break; 
                }
            }        
        }
        if(condicao_parada ==1)
            break; 
        else if(cond ==1){
            Carrinho carrinho = compra(produtos); 
            int socio_verificado = verificacao_de_socio(clientes, cpf); 
            carrinho.imprimir_compras(socio_verificado);
            cout << endl << endl;  
            if(socio_verificado == 1){
                while(true){
                    cout << "(1)Utilizar senha de sócio" << endl << "(2)Continuar a compra sem desconto" << endl; 
                    int escolha_compra = leitura <int> (); 
                    system ("clear"); 
                    if(escolha_compra == 1){
                        while(true){
                            cout << "Digite a senha: ";
                            string senha = lerstring(); 
                            system("clear"); 
                            if(senha == clientes[guardar]->get_senha()){
                                carrinho.set_total_comprado(carrinho.get_total_comprado()*0.85); 
                                break; 
                            }
                            else {
                                int saida; 
                                while(true){
                                    cout << "Senha incorreta!\n\n(1)Tentar novamente\n(2)Continuar a compra" << endl; 
                                    saida = leitura<int> (); 
                                    system("clear"); 
                                    if(saida == 1 || saida ==2)
                                        break;
                                }
                                if(saida ==2)
                                    break; 
                            }
                        }
                        break; 
                    }
                    else if(escolha_compra == 2)
                        break;  
                } 
            }
            else{
                cout << endl << endl; 
                ok(1); 
            }
            clientes[guardar]->set_total_de_compras(clientes[guardar]->get_total_de_compras()+carrinho.get_total_comprado());
            int qntd_produto = produtos.size(), qntd_comprados = carrinho.get_quantidade_itens(); 
            int categorias_existentes = categorias.size(); 
            for(int j=0; j<qntd_comprados;j++){
                for (int k =0; k< qntd_produto; k++){
                    if(produtos[k]->get_nproduto()==carrinho.get_nome_produto(j)){
                        produtos[k]->set_qntd(produtos[k]->get_qntd()-carrinho.get_qntd_compra(j));
                        int qntd_categoriasp = produtos[k]->qntd_de_categorias(); 
                        for(int l=0; l< qntd_categoriasp; l++){
                            for(int m =0; m< categorias_existentes; m++){
                                if(produtos[k]->get_categoria(l) == categorias[m]){
                                    clientes[guardar]->set_categorias_compradas(m, clientes[guardar]->get_categorias_compradas(m)+carrinho.get_qntd_compra(j)); 
                                    break; 
                                }
                            }
                        }
                    }
                }
            }
            system("clear"); 
            cout << "Compra realizada" << endl ; 
            ok(1); 
             break;  
        }
    } 
}

int verificacao_de_socio (vector <Cliente *> clientes, long int cpf){
    int qntd_clientes = clientes.size();
    for (int i =0; i< qntd_clientes; i++){
        if(cpf == clientes[i]->get_cpf()){
            if(clientes[i]->eh_socio() == "sim")
                return 1;
        }
    }
    return 0; 
}

Carrinho compra(vector <Produto *> produtos){
    int i = produtos.size(),cont=0,j,cond,posicao; 
    Carrinho carrinho;
    vector <string> nome_produto; 
    vector <long int> qntd_compra;
    float total_comprado =0;
     cout << "INICIANDO A COMPRA" << endl << endl; 
    while(true) {
        
        if(cont != 0)
        cout << "NOVO ITEM" << endl << endl; 
        while(true){
            cond = 0; 
            cout << "Item de compra: ";
            nome_produto.push_back(lerstring());
            for(j=0;j<cont;j++){
                if(nome_produto[j] == nome_produto[cont]){
                    nome_produto.erase(nome_produto.begin() + cont); 
                    cout << "Produto já foi comprado" << endl << endl; 
                    ok(2); 
                    cond=2;
                    break; 
                }
            }
            if(cond == 0) {
                for(j=0;j<i;j++){
                    if(nome_produto[cont] == produtos[j]->get_nproduto()){
                        cond = 1; 
                        posicao=j; 
                        break;
                    }
                }
            }
            if(cond == 1 && produtos[posicao] -> get_qntd()>0)
                break;
            else if(cond != 2) {
                nome_produto.erase(nome_produto.begin()+cont); 
                while(true) {
                    system("clear");
                    cout << endl << endl << "Produto em falta" << endl <<  endl << "(1)Comprar outro produto" << endl;
                    int lixo = leitura<int>(-1);
                    system("clear");
                    if(lixo == 1)
                        break; 
                    else
                        cout << "Comando Inválido! Tente novamente" << endl;
                }
            }
        }
        while(true){
            cout <<"Quantidade sendo comprada: ";
            qntd_compra.push_back(leitura<long int>(1)); 
            if(qntd_compra[cont] > produtos[posicao]->get_qntd()){
                system("clear"); 
                cout << "Quantidade insuficiente no estoque" << endl << "Quantidade atual de "; 
                cout << produtos[posicao]->get_nproduto() << ": " << produtos[posicao]->get_qntd() << endl <<endl;
                qntd_compra.erase(qntd_compra.begin()+cont); 
            }
            else{
                total_comprado+=(produtos[posicao]->get_preco()*qntd_compra[cont]); 
                break; 
            }
        }
        cont++;
        system("clear");
        cout <<"Adicionar novo item de compra?" << endl << endl << "(1) Sim"<< endl << "(2) Não, finalizar compra" << endl; 
        while(true){
            int adicionar = leitura <int >(-1);
            system("clear");
            if(adicionar ==1 )
                break; 
            else if(adicionar == 2){
                cond=3;
                break; 
            }
            else if(adicionar != 1){
                cout << "Comando inválido!" << endl;
                cout <<"Adicionar novo item de compra?" << endl << endl << "(1) Sim"<< endl << "(2) Não, finalizar compra" << endl;
                
            }
        }
        if(cond == 3)
            break; 
    }
    return  carrinho = Carrinho(nome_produto,qntd_compra,total_comprado); 
}
