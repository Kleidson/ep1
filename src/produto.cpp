#include <iostream>
#include <fstream>
#include <string> 
#include "produto.hpp"
using namespace std;
    Produto::Produto(){
        set_nproduto("");
        set_qntd(0);
        set_preco(0);
    }
     Produto::Produto(string nproduto, long int qntd, float preco, vector <string> categoria){
        this -> nproduto = nproduto; 
        this ->qntd = qntd; 
        this -> preco = preco; 
        this -> categoria = categoria; 
    }
     Produto::~Produto(){

     }

     string Produto::get_nproduto(){
         return nproduto;
     }
     void Produto::set_nproduto(string nproduto){
         this->nproduto=nproduto;
     }

     long int Produto:: get_qntd(){
         return qntd;
     }
     void Produto:: set_qntd(long int qntd){
         this->qntd=qntd;
     }

     float Produto:: get_preco(){
         return preco;
     }
     void Produto:: set_preco(float preco){
         this->preco=preco;
     }
     string Produto ::  get_categoria(int i){
         return categoria[i]; 
     }
     int Produto :: qntd_de_categorias(){
         return categoria.size(); 
     }
    void Produto :: salve () {
        fstream arquivo;
        int qntd_categorias = categoria.size(); 
        arquivo.open("data/produtos.txt", ios :: out | ios :: app);
        arquivo << nproduto << endl;
        arquivo << qntd << endl;
        arquivo << preco << endl; 
        arquivo << qntd_categorias << endl; 
        for(int i= 0; i< qntd_categorias;i++){
            arquivo << categoria[i] << endl; 
        }
        arquivo.close(); 
    }
     void Produto:: imprime_dados(){
         cout << "Nome do produto: " << nproduto << endl;
         cout << "Quantidade no estoque: " << qntd << endl;
         cout << "Preço: R$" << preco << endl;
         int i = categoria.size(); 
         if(i==1)
            cout << "Catergoria: ";
        else 
            cout << "Categorias: " << endl; 
         for(int j=0;j<i;j++){
             cout << categoria[j] << endl; 
         }
         cout << "---------------------------------- "<< endl; 

     }