#include "aux_recomenda.hpp"
#include "aux_estoque.hpp"
#include <algorithm>

using namespace std;
template <typename var>
var leitura (var cond){
    while(true){
    var lido;
    cin >> lido; 
    if(cin.fail() || lido < cond){
        cin.clear();
        cin.ignore(32776, '\n');
        cout << "Entrada Inválida\n"; 
    }
    else 
    return lido; 
    }
}

void recomendacoes(vector<Cliente *>clientes,vector<Produto *> produtos, vector <string> categorias){
    long int cpf;
    int qntd_clientes = clientes.size(), cond=0,guardar; 
    while(true){
        cout << "MODO DE RECOMENDAÇÂO" << endl << endl << endl;
        cout << "Insira o cpf do cliente: ";
            cpf = leitura<long int>(0); 
            system("clear"); 
            for(int i =0; i< qntd_clientes; i++){
                if(cpf == clientes[i]->get_cpf()){
                    cond = 1; 
                    guardar=i; 
                    break; 
                }
            }
            if(cond == 0){
               while(true){
                    cout << "Cliente não cadastrado" << endl << "Tentar novamente?" << endl << endl;
                    cout << "(1)Sim" << endl << "(2)Não " << endl; 
                    int escolha = leitura<int>(0);
                    system("clear"); 
                    if(escolha == 2){
                        cond =2 ;
                        break; 
                    }
                    else if(escolha==1)
                        break;          
               }
            }
            if(cond !=0)
                break;  
    }
    if(cond == 1){
        int qntd_categorias = categorias.size(), qntd_produtos = produtos.size();
        int maior = clientes[guardar]->get_categorias_compradas(0),i;
        vector <string> mais_comprada;  
        vector<int> graus_categoria,graus_ordenados,graus; 
         for( i =0; i<qntd_categorias; i++){
            graus_categoria.push_back(clientes[guardar]->get_categorias_compradas(i));
            graus_ordenados.push_back(clientes[guardar]->get_categorias_compradas(i));
        }  
        for( i =0; i<qntd_categorias; i++){
            if(clientes[guardar]->get_categorias_compradas(i)>maior){
                maior = clientes[guardar]->get_categorias_compradas(i);
            }
        }
        if(maior==0)
            cout << "Cliente não possui compras" << endl; 
        else {
            for( i =0; i<qntd_categorias; i++){
                if(clientes[guardar]->get_categorias_compradas(i)==maior){
                    mais_comprada.push_back(categorias[i]);
                }
            }
        sort(mais_comprada.begin(), mais_comprada.end(), [](string a, string b){return a < b;});
            int aux = mais_comprada.size();
            cout << "Para " << clientes[guardar]->get_nome() << ", recomenda-se produtos ";
            if(aux ==1)
                cout << "do tipo: " << mais_comprada[0] << endl;
            else {
                cout << " das seguintes categorias:" <<endl;
                for(i =0; i<aux ; i++){
                    cout << "- " << mais_comprada[i] << endl;  
                }
            }
            cout << endl << endl; ok(1);
            while(true){
                cout << "Imprimir lista de produtos recomendados?" << endl << endl;
                cout << "(1)Sim" << endl << "(2)Não" << endl; 
                int ordem = leitura<int>(-1); 
                system("clear");
                if(ordem == 1){
                    vector <string> produtos_recomendados;
                    mais_comprada.clear();  
                    int cont = 0, qntd_graus = graus_categoria.size();
                    sort (graus_ordenados.begin(),graus_ordenados.end(),[](int a, int b){return a >b;} );
                    for(int r =0; r< qntd_graus ; r++){
                        for(int s =0; s< qntd_graus ; s++){
                            if(graus_ordenados[r] == graus_categoria[s] && graus_ordenados[r]!=0){
                                int verifica =0;
                                aux = mais_comprada.size();
                                for(int v =0; v< aux; v++){
                                    if(categorias[s]== mais_comprada[v]){
                                        verifica = 1;
                                        break; 
                                    }
                                }
                                if(verifica ==0 ){
                                mais_comprada.push_back(categorias[s]);
                                graus.push_back(graus_ordenados[r]);
                                } 
                            }
                        }
                    }
                    cout << "Lista de produtos recomendados para " << clientes[guardar]->get_nome() << ":" << endl; 
                    aux = mais_comprada.size(); 
                    for(int k =0; k<aux; k++){ 
                        if(graus_ordenados[0] == graus[k])
                             sort(produtos_recomendados.begin(),produtos_recomendados.end(),[](string a, string b){return a < b;});
                        for(i = 0;i<qntd_produtos; i++){
                            if(cont == 10)
                                break;
                            int numero_de_categorias = produtos[i]->qntd_de_categorias();
                            for(int j = 0; j<numero_de_categorias;j++){
                                if(mais_comprada[k]==produtos[i]->get_categoria(j)){
                                    int repeticao = 0, qntd_recomendados = produtos_recomendados.size(); 
                                    for(int ver =0; ver < qntd_recomendados ; ver++){
                                        if(produtos[i]->get_nproduto() == produtos_recomendados[ver]){
                                           repeticao =1; 
                                           break;  
                                        }
                                    }
                                    if(repeticao ==0 ) {
                                    produtos_recomendados.push_back(produtos[i]->get_nproduto()); 
                                    cont ++; 
                                    break;
                                    } 
                                }
                            }
                        }        
                    }
                    
                    int qntd_produtos_recomendados = produtos_recomendados.size(); 
                    for(int j = 0; j<qntd_produtos_recomendados; j++ )
                        cout << "-" << produtos_recomendados[j] << endl;  
                    cout << endl << endl; ok(2); 
                    break;   
                }
                else if(ordem ==2)
                    break;
            }
        }
    }
}