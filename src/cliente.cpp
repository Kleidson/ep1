#include <iostream> 
#include <fstream>
#include <string>
#include "cliente.hpp" 
using namespace std;
template <typename var>
var leitura (){
    while(true){
    var lido;
    cin >> lido; 
    if(cin.fail()){
        cin.clear();
        cin.ignore(32776, '\n');
        cout << "Entrada Inválida\n"; 
    }
    else 
    return lido; 
    }
}
   Cliente:: Cliente (){
       set_nome("");
       set_cpf(0);
       set_email("@gmail.com");
       set_total_de_compras(0); 
       set_senha(""); 

   }
   Cliente :: Cliente (long int cpf){
       set_nome("");
       set_cpf(cpf);
       set_email("@gmail.com");
       set_total_de_compras(0); 
       set_senha("");

   }
   Cliente :: Cliente(string nome, long int cpf, string email){
       this -> nome = nome;
       this -> cpf = cpf;
       this -> email = email; 
       set_total_de_compras(0); 
       set_senha("");

   }
  Cliente ::Cliente(string nome, long int cpf, float total_de_compras,string email){
    this -> nome = nome;
       this -> cpf = cpf;
       this -> email = email; 
       this -> total_de_compras = total_de_compras;
       set_senha("");
   }
   Cliente :: Cliente(string nome, long int cpf, vector <int> categorias_compradas,  float total_de_compras, string email){
       this -> nome = nome;
       this -> cpf = cpf;
       this -> email = email; 
       this -> total_de_compras = total_de_compras;
       set_senha("");
       this -> categorias_compradas = categorias_compradas; 
   }
   
    Cliente:: ~Cliente(){

    }

    void Cliente:: set_nome(string nome){
        this-> nome = nome;
    }
    string Cliente:: get_nome(){
        return nome;
    }
    void Cliente:: set_cpf(long int cpf){
        this-> cpf = cpf;
    }
    long int Cliente:: get_cpf(){
        return cpf;
    }
    void Cliente:: set_email(string email){
        this->email = email;
    }
    string Cliente:: get_email(){
        return email;
    }
    void Cliente :: set_senha(string senha){
        this -> senha = senha;
    }
    string Cliente:: get_senha(){
        return senha; 
    }
    void Cliente::  set_total_de_compras(float total_de_compras){
        this-> total_de_compras = total_de_compras;
    }
    float Cliente :: get_total_de_compras(){
        return total_de_compras; 
    }
    void Cliente :: set_categorias_compradas( int i, int vezes){
        categorias_compradas[i] = vezes; 
    }
    int Cliente :: get_categorias_compradas(int i){
        return categorias_compradas[i];
    }

    int Cliente :: categorias_existentes() {
        return categorias_compradas.size(); 
    }
    void Cliente :: adicionar_categoria(int faltam){
        for(int i = 0; i< faltam; i++)
        categorias_compradas.push_back(0); 
    }
    
   void Cliente :: salvar(int total_de_categorias){
       ofstream arquivo; 
       arquivo.open("data/clientes.txt", ios:: out | ios:: app);
       arquivo << nome << endl; 
       arquivo << cpf << endl; 
       arquivo << email << endl;
       arquivo << total_de_compras << endl;
       arquivo << total_de_categorias << endl;
       for(int i =0; i< total_de_categorias; i++){
           arquivo << categorias_compradas[i] << endl; 
       }
       arquivo << "-" <<endl;
       arquivo << "-" << endl;  
       arquivo.close();
   }
    string Cliente :: eh_socio(){
        return "não"; 
    }
    void Cliente::imprime_dados(){
        cout << "Nome: " << nome << endl;
        cout << "CPF: " ;
        if(cpf / 10000000000 == 0) 
            cout << "0";  
        cout << cpf << endl;
        cout << "Email: " << email << endl;
        cout << "Sócio: Não" << endl; 
        cout << "Total comprado: R$" << total_de_compras << endl; 
        cout << endl << "-------------------------------" << endl;
        
    }