#include <iostream> 
#include <fstream>
#include "socio.hpp"
 
       Socio:: Socio(){
        set_nome("");
        set_cpf(0);
        set_email("@gmail.com");
        set_total_de_compras(0); 
        data_de_cadastro = "0/0/0";
        senha = "123456"; 
        }
        Socio::Socio(string nome, long int cpf, string email,float total_de_compras, string data_de_cadastro,string senha){
            set_nome(nome);
            set_cpf(cpf);
            set_email(email);
            set_total_de_compras(total_de_compras);
            set_data(data_de_cadastro);
            set_senha(senha);
        }
        Socio::Socio(string nome, long int cpf, string email, string data_de_cadastro,string senha){
            set_nome(nome);
            set_cpf(cpf);
            set_email(email);
            set_total_de_compras(0);
            set_data(data_de_cadastro);
            set_senha(senha);
        }
        Socio :: Socio(string nome, long int cpf, string email,vector <int> categorias_compradas, float total_de_compras, string data_de_cadastro,string senha){
            set_nome(nome);
            set_cpf(cpf);
            set_email(email);
            set_total_de_compras(total_de_compras);
            set_data(data_de_cadastro);
            set_senha(senha);
            this-> categorias_compradas = categorias_compradas; 
        }
        Socio:: ~Socio(){

        } 

        void Socio::  set_data(string data_de_cadastro){
            this -> data_de_cadastro = data_de_cadastro;
        }
        string Socio::get_data(){
            return data_de_cadastro;
        }
        string Socio :: eh_socio() {
            return "sim"; 
        }
        void Socio :: salvar(int total_de_categorias){
            ofstream arquivo; 
            arquivo.open("data/clientes.txt", ios:: out | ios:: app);
            arquivo << nome << endl; 
            arquivo << cpf << endl; 
            arquivo << email << endl;
            arquivo << total_de_compras << endl;
            arquivo << total_de_categorias << endl;
            for(int i =0; i< total_de_categorias; i++){
                arquivo << categorias_compradas[i] << endl; 
            }  
            arquivo << data_de_cadastro << endl; 
            arquivo << get_senha() << endl; 
            arquivo.close();
   }
        void Socio :: imprime_dados(){
            cout << "Nome: " << nome << endl;
            cout << "CPF: " ;
            if(cpf / 10000000000 == 0) 
                cout << "0";  
            cout << cpf << endl;
            cout << "Email: " << email << endl;
            cout << "Sócio: Sim" << endl; 
            cout << "Sócio cadastrado no dia:" << data_de_cadastro <<  endl;
            cout << "Total comprado: R$" << total_de_compras << endl;  
            cout << endl << "-------------------------------" << endl;
        
        }