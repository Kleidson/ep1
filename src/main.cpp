#include <iostream> 
#include <string>
#include <vector> 
#include <fstream> 
#include "produto.hpp"
#include "cliente.hpp"
#include "socio.hpp"
#include "cadastro_cliente.hpp"
#include "carrinho.hpp" 
#include "aux_compra.hpp"
#include "aux_estoque.hpp"
#include "cadastro_socio.hpp"
#include "aux_recomenda.hpp"


using namespace std;
template <typename var>
var leitura (){
    while(true){
    var lido;
    cin >> lido; 
    if(cin.fail()){
        cin.clear();
        cin.ignore(32776, '\n');
        cout << "Entrada Inválida\n"; 
    }
    else 
    return lido; 
    }
}
 
int main (){
    system("clear");
    fstream arquivo_cliente, arquivo_produto,arquivo_categorias;  
    int escolha=-1, i,numero_clientes, numero_produtos,numero_categorias,qntd_categorias,compra_categoria; 
    long int cpf,qntd;
    int total_de_categorias; 
    string nome, email,nome_produto,guardar_categoria,teste,data_de_cadastro,senha,categorias_arquivo;
    float total_de_compras,preco; 
    vector <string> categorias_do_arquivo, categorias_cadastradas;  
    vector<int>categorias_compradas; 
    vector <Produto *>  produtos;
    vector <Cliente *>  clientes;

    arquivo_cliente.open("data/clientes.txt");
    while(getline(arquivo_cliente,nome)){
        arquivo_cliente >> cpf;
        arquivo_cliente >> email;
        arquivo_cliente>> total_de_compras;
        arquivo_cliente >> total_de_categorias;
        for(int contador =0; contador <total_de_categorias; contador ++){
            arquivo_cliente >> compra_categoria; 
            categorias_compradas.push_back(compra_categoria); 
        }  
        arquivo_cliente>> data_de_cadastro;
        arquivo_cliente>> senha;
        arquivo_cliente.ignore(); 
        if(data_de_cadastro == "-")
            clientes.push_back(new Cliente(nome,cpf,categorias_compradas,total_de_compras,email));
        else
            clientes.push_back(new Socio(nome,cpf,email,categorias_compradas,total_de_compras, data_de_cadastro,senha));
        categorias_compradas.clear();
    }
    arquivo_cliente.close(); 

    arquivo_produto.open("data/produtos.txt");
    while(getline(arquivo_produto,nome_produto)){
        arquivo_produto >> qntd;
        arquivo_produto >> preco;
        arquivo_produto >> qntd_categorias;
        arquivo_produto.ignore();
        for(i=0;i<qntd_categorias;i++){ 
            getline(arquivo_produto,guardar_categoria);
            categorias_do_arquivo.push_back(guardar_categoria); 
        }
        produtos.push_back(new Produto(nome_produto, qntd, preco, categorias_do_arquivo));
        categorias_do_arquivo.clear();        
    }
    arquivo_produto.close();
    arquivo_categorias.open("data/categorias",ios :: in | ios :: app);
    while(getline(arquivo_categorias,categorias_arquivo)){ 
        categorias_cadastradas.push_back(categorias_arquivo); 
    }
    arquivo_categorias.close(); 

    while(escolha!=0){
    cout << "Escolha o modo:" << endl << endl << endl;
    cout << "(1) Venda" << endl << "(2) Estoque" << endl << "(3) Recomendação" << endl << "(0) Finalizar programa" << endl; 
   
    escolha = leitura <int>(); 
    system("clear"); 
    switch (escolha){
        case 1 : {
            while (true){
                cout << "MODO VENDA" << endl << "Escolha uma opção:" << endl << endl << endl; 
                cout << "(1) Carrinho" << endl << "(2) Cadastro"  << endl; 
                cout << "(3) Imprimir todos os clientes cadastrados" << endl  <<  "(0) Retornar ao menu principal" << endl << endl; 
                int opcao_venda = leitura<int>();
                system("clear");
                if(opcao_venda == 1){
                    if(produtos.size()==0){
                        cout << "Não há produtos para vender!" << endl << endl ;
                        ok(2);
                    }    
                    else 
                        verificacao_de_cliente(clientes,produtos,categorias_cadastradas); 
                }
               else if(opcao_venda == 2){ 
                    while(true){
                        cout << "MODO VENDA -- CADASTRO" << endl << endl << "(1)Cadastre um cliente" << endl << "(2)Cadastre um sócio" <<endl; 
                        cout << "(3)Atualizar dados de cliente" << endl << "(0)Voltar" <<endl;
                        int opcao_cadastro = leitura<int>();
                        system("clear"); 
                        if(opcao_cadastro == 1){
                        clientes.push_back(new Cliente (cadastre(clientes)));
                        numero_clientes = clientes.size();
                        if(clientes[numero_clientes-1]->get_nome()!="")
                            cout << endl << endl <<  "Cadastro feito com sucesso!" << endl; 
                        ok(1);
                        break; 
                        }
                        else if(opcao_cadastro == 2){
                            int opcao_socio = escolha_opcao();
                            if( opcao_socio == 1)
                               clientes.push_back(new Socio(cadastro_ce(clientes)));
                            else if(opcao_socio == 2)
                                clientes.push_back(new Socio(cadastro_ci(clientes)));
                            if(opcao_socio != 0){
                                ok(2);
                                break;  
                            }
                        }
                        else if(opcao_cadastro == 3)
                            edita_clientes(clientes); 

                        else if(opcao_cadastro == 0)
                            break;      
                    }
                    numero_clientes = clientes.size();
                    if(numero_clientes!=0){ 
                    for(i=0;i<numero_clientes;i++){
                        if(clientes[i]->get_nome()=="")
                            clientes.erase(clientes.begin()+i); 
                    }
                    }
                    atualizacao_clientes(clientes, categorias_cadastradas); 
                }
                else if(opcao_venda == 3){
                    system("clear"); 
                    numero_clientes = clientes.size(); 
                    if(numero_clientes == 0){
                        cout << "Nenhum cliente cadastrado" << endl << endl;
                        ok(2); 
                    }
                    else {
                        cout << "Número de clientes cadastrados: " << numero_clientes << endl << endl; 
                        for(i=0;i<numero_clientes;i++){
                            if(clientes[i]->get_email()!="")
                            clientes[i]->imprime_dados();      
                        }
                        ok(2);   
                    }   
                } 
                else if( opcao_venda == 0)
                    break; 
                else
                    system("clear");   
            } 
            break;
        }
        case 2:  {
            while (true){
            int opcao_estoque; 
            cout << "MODO ESTOQUE " << endl  <<  "O que deseja?" << endl << endl << endl << "(1) Cadastrar produto "<< endl;
            cout <<"(2) listar produtos " << endl <<"(3) Editar estoque" << endl << "(0) Retornar ao menu principal" << endl; 
           opcao_estoque = leitura <int> (); 
            system("clear");  
            if(opcao_estoque== 1){
                produtos.push_back(new Produto(cadastre_p(produtos))); 
                ok(1); 

                numero_produtos=produtos.size();
                if(produtos[numero_produtos-1]->get_nproduto()=="")
                    produtos.erase(produtos.begin()+numero_produtos-1); 

                int numero_de_categorias = categorias_cadastradas.size(); 
                for( i = 0; i< numero_produtos; i++){
                    int qntd_categorias = produtos[i]->qntd_de_categorias(); 
                    for(int j= 0; j< qntd_categorias; j++){
                        int cond = 0;
                        numero_de_categorias = categorias_cadastradas.size();
                        for(int k = 0; k < numero_de_categorias ; k++){
                            if(categorias_cadastradas[k] == produtos[i]->get_categoria(j)){
                                cond = 1;
                                break; 
                            }
                        }
                        if(cond == 0)
                            categorias_cadastradas.push_back(produtos[i]->get_categoria(j));     
                    }
                    }       
            }
            else if(opcao_estoque==2){
                system("clear"); 
                numero_produtos=produtos.size();
                if(numero_produtos==0){
                    cout << "Não há produtos nos estoque" << endl << endl << endl;
                    ok(2); 
                }
                else{
                    for(int i=0;i<numero_produtos;i++) {
                        produtos[i]->imprime_dados(); 
                    }
                    cout << endl << endl;
                    ok(2);
                    system("clear");  
                }   
            }
            else if(opcao_estoque == 3){
                while(true) {
                int altera = edition(produtos);
                if(altera !=-1)
                    produtos.erase(produtos.begin()+altera);
                else 
                    break;  
                }
            }
            else if(opcao_estoque == 0 )
                break;   
            }
            atualizacao_clientes(clientes, categorias_cadastradas); 
            break; 
        }
        case 3 : {
            recomendacoes(clientes,produtos,categorias_cadastradas); 
            break;
        }
    }
    }
    int contador; 
    numero_clientes = clientes.size(); 
    numero_produtos = produtos.size(); 
    arquivo_cliente.open("data/clientes.txt",ios :: out | ios ::trunc);
    arquivo_cliente.close(); 
    arquivo_produto.open("data/produtos.txt",ios :: out | ios ::trunc);
    arquivo_produto.close();
    arquivo_categorias.open("data/categorias", ios :: out | ios :: trunc);
    numero_categorias = categorias_cadastradas.size();
    for(i =0; i< numero_categorias ; i++)
       arquivo_categorias << categorias_cadastradas[i] << endl;
    arquivo_categorias.close(); 

    for( contador = 0; contador < numero_clientes;contador++){
       clientes[contador]->salvar(numero_categorias); 
    }
    for(contador =0; contador < numero_produtos; contador++){
        produtos[contador]->salve(); 
    }
    return 0;
}