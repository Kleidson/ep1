#ifndef AUX_RECOMENDA_HPP
#define AUX_RECOMENDA_HPP

#include "cliente.hpp"
#include "socio.hpp"
#include "produto.hpp"

void recomendacoes(vector<Cliente *>clientes,vector<Produto *> produtos, vector <string> categorias);

#endif 