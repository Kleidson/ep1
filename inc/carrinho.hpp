#ifndef CARRINHO_HPP
#define CARRINHO_HPP
#include <string>
#include <vector>
using namespace std; 
class Carrinho {
private: 
    vector <string> nome_produto;
    vector <long int> qntd_compra;
    float total_comprado;
    int quantidade_itens; 
  public:  
    Carrinho();
    Carrinho(vector <string> nome_produto, vector <long int> qntd_compra,float total_comprado);
    ~Carrinho(); 
    void set_nome_produto(vector <string> nome_produto);
    string get_nome_produto(int i);
    void set_qntd_compra(vector <long int> qntd_compra);
    long int get_qntd_compra(int i);
    void set_total_comprado(float total_comprado);
    float get_total_comprado(); 
    int get_quantidade_itens(); 
    void imprimir_compras(int l);

};

#endif