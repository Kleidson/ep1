#ifndef AUX_COMPRA_HPP
#define AUX_COMPRA_HPP
#include <vector>
#include "cliente.hpp"
#include "produto.hpp"
#include "cadastro_cliente.hpp"
#include "carrinho.hpp"

Carrinho compra(vector <Produto*> produtos);
void verificacao_de_cliente(vector <Cliente *> &clientes, vector <Produto *> produtos,  vector <string> categorias); 
int verificacao_de_socio (vector <Cliente *> clientes, long int cpf); 
int compra_clientecadastrado(vector <Cliente *> clientes, vector <Produto *> produtos, vector <Carrinho *> carrinho, vector<string>categorias); 
void compra_clientenovo(vector <Produto *> produtos, vector <Carrinho *> carrinho, vector<Cliente *> clientes, vector <string> categorias);
#endif
