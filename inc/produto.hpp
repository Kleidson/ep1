#ifndef PRODUTO_HPP
#define PRODUTO_HPP

#include <iostream>
#include <string>
#include <vector>

using namespace std;

 class Produto {
     private: 
     string nproduto;
     long int qntd;
     float preco;
     vector <string> categoria; 
     
    public: 
     Produto();
     Produto(string nproduto, long int qntd, float preco, vector <string> categoria);
     ~Produto();

    string get_nproduto();
    void set_nproduto(string nproduto);
    long int get_qntd();
    void set_qntd(long int qntd);
    float get_preco();
    void set_preco(float preco);
    string get_categoria(int i); 
    int qntd_de_categorias(); 
    void salve(); 
    void imprime_dados();

     
 };
#endif