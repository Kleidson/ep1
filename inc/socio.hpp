#ifndef SOCIO_HPP
#define SOCIO_HPP
#include "cliente.hpp"
#include <string> 

class Socio : public Cliente {
    protected:
        string data_de_cadastro; 
    public: 
        Socio(); 
        Socio(string nome, long int cpf, string email,float total_de_compras, string data_de_cadastro,string senha);
        Socio(string nome, long int cpf, string email,vector <int> categorias_compradas, float total_de_compras, string data_de_cadastro,string senha);
        Socio(string nome, long int cpf, string email, string data_de_cadastro,string senha);
        ~Socio(); 

        void set_data(string data_de_cadastro);
        string get_data();
        virtual string eh_socio(); 
         void salvar(int total_de_categorias); 
        virtual void imprime_dados();

 
};
#endif