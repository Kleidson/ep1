#ifndef CLIENTE_HPP
#define CLIENTE_HPP

#include <iostream> 
#include <string> 
#include <vector>
using namespace std; 
class Cliente {
    protected: 
    string nome;
    long int cpf;
    string email,senha;
    float total_de_compras; 
    vector <int> categorias_compradas; 
    
    public: 
    Cliente ();
    Cliente (long int cpf);
    Cliente(string nome, long int cpf, float total_de_compras, string email);
    Cliente(string nome, long int cpf, vector <int> categorias_compradas,  float total_de_compras, string email);
    Cliente(string nome, long int cpf, string email);
   
    ~Cliente();

    void set_nome(string nome);
    string get_nome();
    void set_cpf(long int cpf);
    long int get_cpf();
    void set_email(string email);
    string get_email();
    void set_senha(string senha);
    string get_senha();
    void set_total_de_compras(float total_de_compras);
    float get_total_de_compras(); 
    void set_categorias_compradas(int i, int vezes);
    int get_categorias_compradas(int i); 
    
    void adicionar_categoria(int faltam);
    int categorias_existentes();  
    virtual void salvar(int total_de_categorias); 
    virtual string eh_socio(); 
    virtual void imprime_dados(); 
}; 
#endif